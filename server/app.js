const express = require('express');
const graphqlHTTP = require('express-graphql');
const schema = require('./schema/schema');
const mongoose = require('mongoose');
const cors = require('cors');

const app = express();

//allow cors
app.use(cors());

//connect to DB
mongoose.connect(process.env.DB_CONNECTION, {useNewUrlParser: true});
mongoose.connection.once('open', () => console.log('connected to graphql DB'));


app.use('/books-api', graphqlHTTP({
    schema,
    graphiql: true
}));

app.listen(4000, () => console.log('now listening port 4000'));
