module.exports = {
    BOOKS: [
        {
            name: 'the tale of birl',
            id: 1,
            genre: 'sci-fi',
            authorId: 3
        },
        {
            name: 'birl for your life',
            id: 2,
            genre: 'horror',
            authorId: 1
        },
        {
            name: 'the last birl',
            id: 3,
            genre: 'fantasy',
            authorId: 2
        },
        {
            name: 'the birl',
            id: 4,
            genre: 'sci-fi',
            authorId: 2
        },
        {
            name: 'birl of the century',
            id: 5,
            genre: 'western',
            authorId: 3
        },
        {
            name: 'birl\'s anthem',
            id: 6,
            genre: 'birl',
            authorId: 3
        }
    ],
    AUTHORS: [
        {
            id: 1,
            name: 'bill',
            age: 32,
        },
        {
            id: 2,
            name: 'bob',
            age: 12,
        },
        {
            id: 3,
            name: 'betty',
            age: 21,
        },
    ]
};
