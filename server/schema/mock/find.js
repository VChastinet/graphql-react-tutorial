function findOne(array, criteria) {
    const key = Object.keys(criteria)[0];
     return array.filter(item => item[key] == criteria[key])[0];
}

function findMany(array, criteria) {
    const key = Object.keys(criteria)[0];
    return array.filter(item => item[key] == criteria[key]);
}

module.exports = {
    findOne: findOne,
    findMany: findMany
};
